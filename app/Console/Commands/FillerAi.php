<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use App\Services\ComputerIntelligenceService;

/**
 * Implementation of console command logic, which used for making step
 */
class FillerAi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'filler_ai:makeStep {--gameServer=} {--gameId=} {--playerId=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command make step in the game';
    /**
     * @var ComputerIntelligenceService
     */
    private $computerIntelliganceService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->computerIntelliganceService = new ComputerIntelligenceService();
        parent::__construct();
    }

    /**
     * Sends a request to the api and makes a move for the selected player
     *
     * @return int
     * @throws \JsonException
     */
    public function handle()
    {
        $gameServer = $this->option('gameServer');
        $gameId = $this->option('gameId');
        $playerId = (int) $this->option('playerId');

        $data = $this->getFieldData($gameServer, $gameId);
        $gamePlayers = $this->getGamePlayers($gameServer, $gameId);

        $winnerId = $this->getGameWinner($gameServer, $gameId);
        if ($winnerId !== 0) {
            exit($winnerId);
        }

        $currentPlayerId = $this->getGameCurrentPlayer($gameServer, $gameId);
        if ($currentPlayerId === $playerId) {
            $bestColor = $this->computerIntelliganceService->selectBestColorAdvanced($playerId, $data, $gamePlayers);
            Http::put("{$gameServer}game/{$gameId}", [
                'playerId' => $playerId,
                'color' => $bestColor,
            ]);
        }

        exit(0);
    }

    /**
     * @param string $gameServer
     * @param string $gameId
     * @return array
     *
     * @throws \JsonException
     */
    public function getFieldData(string $gameServer, string $gameId): array
    {
        $response = Http::get("{$gameServer}game/{$gameId}");
        $data = json_decode($response, true, 512, JSON_THROW_ON_ERROR);

        return [
            'width' => (int) $data['field']['width'],
            'height' => (int) $data['field']['height'],
            'cells' => $data['field']['cells']
        ];
    }

    /**
     * @param string $gameServer
     * @param string $gameId
     * @return int
     * @throws \JsonException
     */
    public function getGameCurrentPlayer(string $gameServer, string $gameId): int
    {
        $response = Http::get("{$gameServer}game/{$gameId}");
        $data = json_decode($response, true, 512, JSON_THROW_ON_ERROR);

        return (int) $data['currentPlayerId'];
    }

    /**
     * @param string $gameServer
     * @param string $gameId
     *
     * @return int
     * @throws \JsonException
     */
    public function getGameWinner(string $gameServer, string $gameId): int
    {
        $response = Http::get("{$gameServer}game/{$gameId}");
        $data = json_decode($response, true, 512, JSON_THROW_ON_ERROR);

        return (int) $data['winnerPlayerId'];
    }

    /**
     * @param string $gameServer
     * @param string $gameId
     *
     * @return mixed
     * @throws \JsonException
     */
    public function getGamePlayers(string $gameServer, string $gameId)
    {
        $response = Http::get("{$gameServer}game/{$gameId}");
        $data = json_decode($response, true, 512, JSON_THROW_ON_ERROR);

        return $data['players'];
    }
}
