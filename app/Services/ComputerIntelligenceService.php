<?php

namespace App\Services;

use Exception;

/**
 * Implementation of logic for computer mind and his strategies
 */
class ComputerIntelligenceService
{
    /**
     * Get neighboring conditions for selected cell (count: from 1 to 4)
     *
     * @param int $number - selected cell number
     * @param int $width  - current game field width
     * @param int $count  - count cells of current game field
     * @return array - neighboring conditions (cases)
     */
    public function getNeighboringCases(int $number, int $width, int $count): array
    {
        $cases = [];
        if ($number % (($width * 2) - 1) === 0) {
            if ($number + $width < $count) {
                $cases[] = $number + $width;
            }
            if ($number - ($width - 1) >= 0) {
                $cases[] = $number - ($width - 1);
            }
        } elseif (($number - ($width - 1)) % (($width * 2) - 1) === 0) {
            if ($number + $width - 1 < $count) {
                $cases[] = $number + $width - 1;
            }
            if ($number - $width >= 0) {
                $cases[] = $number - $width;
            }
        } else {
            if ($number + $width < $count) {
                $cases[] = $number + $width;
            }
            if ($number - ($width - 1) >= 0) {
                $cases[] = $number - ($width - 1);
            }
            if ($number + $width - 1 < $count) {
                $cases[] = $number + $width - 1;
            }
            if ($number - $width >= 0) {
                $cases[] = $number - $width;
            }
        }
        return $cases;
    }

    /**
     * Get neighboring cells of selected cell (count: from 1 to 4)
     *
     * @param int   $number    - number of selected cell
     * @param array $fieldData - array with data about current game field
     *
     * @return array
     */
    public function getNeighboringCells(int $number, array $fieldData): array
    {
        $cases = $this->getNeighboringCases($number, $fieldData['width'], count($fieldData['cells']));
        $cells = [];
        foreach ($cases as $case) {
            if (isset($fieldData['cells'][$case])) {
                $cells[$case] = $fieldData['cells'][$case];
            }
        }
        return $cells;
    }

    /**
     * Capture new cells for selected players
     *
     * @param array $cell      - selected cell data (color code and player_id)
     * @param int   $number    - number of selected cell
     * @param int   $playerId  - selected player id
     * @param array $fieldData - array with data about current game field
     *
     * @return array - updated game field data
     */
    public function assignCellsToPlayerRecursive(array $cell, int $number, int $playerId, array $fieldData): array
    {
        $neighboringCells = $this->getNeighboringCells($number, $fieldData);
        $neighboringCellsNextColor = [];
        foreach ($neighboringCells as $key => $neighboringCell) {
            if ($neighboringCell['color'] == $cell['color']) {
                $neighboringCellsNextColor[$key] = $neighboringCell;
            }
        }

        if (empty($neighboringCellsNextColor)) {
            return $fieldData;
        }

        foreach ($neighboringCellsNextColor as $key => $neighboringCell) {
            if ($neighboringCell['playerId'] != $playerId) {
                $fieldData['cells'][$key]['playerId'] = $playerId;
                $fieldData = $this->assignCellsToPlayerRecursive($fieldData['cells'][$key], $key, $playerId, $fieldData);
            }
        }
        return $fieldData;
    }

    /**
     * Returns all current player cells
     *
     * @param int   $playerId  - selected player id
     * @param array $fieldData - array with data about current game field
     *
     * @return array
     */
    public function getCellsByPlayerId(int $playerId, array $fieldData): array
    {
        $cells = [];
        foreach ($fieldData['cells'] as $key => $cell) {
            if ($cell['playerId'] == $playerId) {
                $cells[$key] = $cell;
            }
        }
        return $cells;
    }

    /**
     * Update color for selected cells
     *
     * @param array  $cells     - cells for update
     * @param string $nextColor - applied color
     * @param array  $fieldData - array with data about current game field
     */
    public function changeCellsColor(array &$cells, string $nextColor, array &$fieldData): void
    {
        foreach ($cells as $key => $cell) {
            $fieldData['cells'][$key]['color'] = $nextColor;
            $cells[$key]['color'] = $nextColor;
        }
    }

    /**
     * Simulate step in the game data for estimation how many points a certain color can bring
     *
     * @param int    $playerId  - the number of the player who make step
     * @param string $nextColor - color which will be applied after this step
     * @param array  $fieldData - array with game field data, especially: width, height, cells
     *
     * @return array
     */
    public function simulateStep(int $playerId, string $nextColor, array $fieldData): array
    {
        $currentUserCells = $this->getCellsByPlayerId($playerId, $fieldData);
        $this->changeCellsColor($currentUserCells, $nextColor, $fieldData);

        foreach ($currentUserCells as $number => $cell) {
            $fieldData = $this->assignCellsToPlayerRecursive($cell, $number, $playerId, $fieldData);
        }

        return $fieldData;
    }

    /**
     * Returns best color (color which will bring the most points) for certain player
     * Middle difficulty computer level
     *
     * @param int   $playerId    - selected player id
     * @param array $fieldData   - array with game field data, especially: width, height, cells
     * @param array $gamePlayers - array with data about game players (first player and second)
     *
     * @return string
     */
    public function selectBestColor(int $playerId, array $fieldData, array $gamePlayers): string
    {
        $availableColors = $this->getAvailableColors($gamePlayers);

        $scores = [];
        foreach ($availableColors as $color) {
            $data = $this->simulateStep($playerId, $color, $fieldData);
            $scores[$color] = count($this->getCellsByPlayerId($playerId, $data));
        }

        return array_keys($scores, max($scores))[0];
    }

    /**
     * Returns available colors for current game state
     *
     * @param array $gamePlayers - array with data about game players (first player and second)
     *
     * @return string[]
     */
    public function getAvailableColors(array $gamePlayers): array
    {
        $bookedColors = [];
        $allColors = ["#0000ff", "#00ff00", "#00ffff", "#ff0000", "#ff00ff", "#ffff00", "#ffffff"];
        foreach ($gamePlayers as $gamePlayer) {
            $bookedColors[] = $gamePlayer['color'];
        }
        foreach ($allColors as $key => $value) {
            if (in_array($value, $bookedColors)) {
                unset($allColors[$key]);
            }
        }
        return $allColors;
    }

    /**
     * Returns best color (color which will bring the most points taking into account the possibility of
     * capturing neighboring cells by the enemy) for certain player
     *
     * The most difficult computer level (default)
     *
     * @param int   $playerId    - selected player id
     * @param array $fieldData   - array with game field data, especially: width, height, cells
     * @param array $gamePlayers - array with data about game players (first player and second)
     *
     * @return string
     */
    public function selectBestColorAdvanced(int $playerId, array $fieldData, array $gamePlayers): string
    {
        $availableColors = $this->getAvailableColors($gamePlayers);

        $scores = [];
        $oldPlayerCells = $this->getCellsByPlayerId($playerId, $fieldData);

        $enemyId = 1;
        if ($playerId === 1) {
            $enemyId = 2;
        }

        foreach ($availableColors as $color) {
            $data = $this->simulateStep($playerId, $color, $fieldData);
            $enemyData = $this->simulateStep($enemyId, $color, $fieldData);

            $currentPlayerCells = $this->getCellsByPlayerId($playerId, $data);
            $capturedCells = array_filter($currentPlayerCells, static function($key) use ($oldPlayerCells) {
                return !array_key_exists($key, $oldPlayerCells);
            }, ARRAY_FILTER_USE_KEY);
            $capturedCellsScore = 0;
            foreach ($capturedCells as $key => $capturedCell) {
                $capturedCellsScore += $this->assignScoreToCell($key, $enemyData, $enemyId);
            }
            $scores[$color] = $capturedCellsScore;
        }

        return array_keys($scores, max($scores))[0];
    }

    /**
     * Assign priority for selected cell, if enemy can capture this cell, priority rises
     *
     * @param int   $number    - cell number on the field
     * @param array $enemyData - game field state after his step
     * @param int   $enemyId   - the number of the player who will move after the current one
     *
     * @return int - score value
     */
    public function assignScoreToCell(int $number, array $enemyData, int $enemyId): int
    {
        $score = 1;

        if (isset($enemyData['cells'][$number]) && $enemyData['cells'][$number]['playerId'] === $enemyId) {
            $score++;
        }

        return $score;
    }

    /**
     * Returns random color for player step (can return color that not a single cell can capture)
     * easy computer mode
     *
     * @param array $gamePlayers - array with data about game players (first player and second)
     *
     * @return string - random color from available colors
     * @throws Exception
     */
    public function selectRandomColor(array $gamePlayers): string
    {
        $availableColors = $this->getAvailableColors($gamePlayers);
        $availableColors = array_values($availableColors);
        $color = '';
        $index = random_int(0, 4);
        foreach ($availableColors as $key => $value) {
            if ($index === $key) {
                $color = $value;
            }
        }
        return $color;
    }
}
