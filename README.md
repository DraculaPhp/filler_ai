## Описание
Это задание было выполнено в рамках финального этапа олимпиады Волга-IT.
Задание представляет из себя консольную команду, которая делает наилучший ход за выбранного игрока
на основе анализа текущего состояния игры.
Доступно 3 уровня сложности:

Easy level - компьютер будет выбирать абсолютно рандомный цвет 
```
public function selectRandomColor(array $gamePlayers)
```
Middle level - компьютер будет выбирать цвет на основе количества захзваченных клеток
```
public function selectBestColor(int $playerId, array $fieldData, array $gamePlayers)
```
Hard level (default) - компьютер будет выбирать цвет, который приносит наибольшее количество очков,
при этом анализируя возможный вражеский ход и расставляя приоритеты для клеток в зависимости
от возможности захзвата их противником.
```
public function selectBestColorAdvanced(int $playerId, array $fieldData, array $gamePlayers)
```

Для выбора уровни сложности нужно поменять соответсвующую функцию внутри метода handle().

## Установка (Локально)
Необходимые компоненты (Linux):
* Настроенный Apache + PHP + MySQL
* [Composer](https://getcomposer.org/)

Склонируйте репозиторий командой:
```
git clone https://gitlab.com/DraculaPhp/filler_ai.git
```

Установите зависимости командой:
```
composer install
```

Выполните:
```
cp .env.example .env
```

Также обязательно не забудьте выполнить:
```
php artisan key:generate
```

Запустите сервер командой:
```
php artisan serve
```

Для запуска тестов используйте:

```
php artisan test
```
Для выполнения хода компьютером запустите:
```
php artisan filler_ai:makeStep --gameServer=http://volga-it-2021.ml/api/ --gameId="617b9b1de9df051bb57d2ee6" --playerId=2
```
Перед этим поменяйте параметры на те, которые вам необходимы.
