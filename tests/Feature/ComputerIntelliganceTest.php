<?php

namespace Tests\Feature;

use App\Services\ComputerIntelligenceService;
use App\Console\Commands\FillerAi as Computer;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ComputerIntelliganceTest extends TestCase
{
    /**
     * @var string[]
     */
    private $allColors;
    /**
     * @var string
     */
    private $gameServer;
    /**
     * @var Computer
     */
    private $computer;
    /**
     * @var string
     */
    private $gameId;
    /**
     * @var ComputerIntelligenceService
     */
    private $computerIntelligence;

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        $this->computerIntelligence = new ComputerIntelligenceService();
        $this->computer = new Computer();
        $this->gameId = '617b9b1de9df051bb57d2ee6';
        $this->gameServer = 'http://volga-it-2021.ml/api/';
        $this->allColors = ["#0000ff", "#00ff00", "#00ffff", "#ff0000", "#ff00ff", "#ffff00", "#ffffff"];
        parent::__construct($name, $data, $dataName);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     * @throws \JsonException
     */
    public function test_select_best_collor_advanced()
    {
        $data = $this->computer->getFieldData($this->gameServer, $this->gameId);
        $gamePlayers = $this->computer->getGamePlayers($this->gameServer, $this->gameId);

        $color = $this->computerIntelligence->selectBestColorAdvanced(2, $data, $gamePlayers);
        $this->assertContains($color, $this->allColors);

        $color = $this->computerIntelligence->selectBestColorAdvanced(1, $data, $gamePlayers);
        $this->assertContains($color, $this->allColors);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     * @throws \JsonException
     */
    public function test_select_best_collor()
    {
        $data = $this->computer->getFieldData($this->gameServer, $this->gameId);
        $gamePlayers = $this->computer->getGamePlayers($this->gameServer, $this->gameId);

        $color = $this->computerIntelligence->selectBestColor(2, $data, $gamePlayers);
        $this->assertContains($color, $this->allColors);

        $color = $this->computerIntelligence->selectBestColor(1, $data, $gamePlayers);
        $this->assertContains($color, $this->allColors);
    }
}
