<?php

namespace Tests\Feature;

use App\Console\Commands\FillerAi as Computer;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Http;
use Tests\TestCase;

class FillerAiTest extends TestCase
{
    /**
     * @var string
     */
    private $gameId;
    /**
     * @var string
     */
    private $gameServer;
    /**
     * @var Computer
     */
    private $computer;

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        $this->computer = new Computer();
        $this->gameId = '617b9b1de9df051bb57d2ee6';
        $this->gameServer = 'http://volga-it-2021.ml/api/';
        parent::__construct($name, $data, $dataName);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function test_get_game_field_data()
    {
        $computer = new Computer();
        $gameServer = 'http://volga-it-2021.ml/api/';
        $gameId = '617b9b1de9df051bb57d2ee6';
        $data = $computer->getFieldData($gameServer, $gameId);
        $this->assertNotEmpty($data);
    }

//    public function test_handle()
//    {
//        $winner = $this->computer->getGameWinner($this->gameServer, $this->gameId);
//        $code = Artisan::call('filler_ai:makeStep', [
//            '--gameServer' => $this->gameServer,
//            '--gameId' => $this->gameId,
//            '--playerId' => 1,
//        ]);
//        if ($winner === 0) {
//            $this->assertEquals(0, $code);
//        } elseif ($winner === 1) {
//            $this->assertEquals(1, $code);
//        } elseif ($winner === 2) {
//            $this->assertEquals(2, $code);
//        } else {
//            $this->assertEquals(0, $code);
//        }
//    }
}
